// node_modules/gulp/bin/gulp.js

var gulp         = require('gulp');
var sass         = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var rename       = require('gulp-rename');
var minifycss    = require('gulp-minify-css');
var notify       = require('gulp-notify');
var cache        = require('gulp-cache');
var imagemin     = require('gulp-imagemin');
var git          = require('gulp-git');
var minifyHTML   = require('gulp-minify-html');
var changed      = require('gulp-changed');
var gutil        = require('gulp-util');
var exec         = require('child_process').exec;
var merge        = require('merge2');
var concat       = require('gulp-concat');
var postcss      = require('gulp-postcss');
var csswring     = require('csswring');
var sourcemap    = require('gulp-sourcemaps');
var runSequence  = require('run-sequence');
var uglify       = require('gulp-uglify');
//var uncss        = require('gulp-uncss');


var configDir = {
    bower:     './bower_components/',
    bootstrap: './bower_components/bootstrap-sass/',
    source:    './source/',
    static:    './static/',
};

var src = {
    root:      './*',
    fonts:     configDir.bootstrap + 'assets/fonts/**/*',
    css_boots: configDir.bootstrap + 'assets/stylesheets',
    sass:      configDir.source + 'css/main.scss',
    images:    configDir.source + 'images/**/*',
    templates: configDir.source + 'templates/**/*.html',
}

var dest = {
    css:       './static/css/',
    fonts:     './static/fonts/',
    images:    './static/images/',
    templates: './templates/',
}


function notifyOK( title ) {
    //gutil.log(gutil.colors.red('test'));
    //gutil.log( 'Traitement OK pour', gutil.colors.green(file.relative), 'in', gutil.colors.magenta(prettyTime));
    //https://github.com/edx/ux-pattern-library/blob/master/gulp/util/bundleLogger.js
    return notify( {
        message: 'Traitement OK pour \'<%= file.relative %>\'',
        title: title,
    });
}


gulp.task('cssedx', function() {
  return gulp.src('/edx/app/edxapp/themes/edx-theme/source/css/edx-sass/lms/static/sass/lms-main.scss')
    .pipe(sass({ includePaths: ['/edx/app/edxapp/themes/edx-theme/static/sass', '/edx/app/edxapp/themes/edx-theme/source/css/edx-sass/common/static/sass', '/edx/app/edxapp/themes/edx-theme/source/css/edx-sass/common/staticok'], /*style: 'compressed'*/ }).on('error', gutil.log))
//    .pipe(gulp.dest(dest.css))
//    .pipe(rename({suffix: '.min'}))
    .pipe(postcss([csswring]))
    //.pipe(sourcemap.write())
//    .pipe(gulp.dest(dest.css))
    .pipe(gulp.dest('/edx/app/edxapp/edx-platform/lms/static/sass/'))
    .pipe( notifyOK('CSS edX') );
});


/*merge(
      gulp.src(src.sass)
        .pipe(sass({ includePaths: [src.css_boots] }).on('error', gutil.log)),
      gulp.src(dest.css+'main.css')
    )*/


gulp.task('css', function() {
  return gulp.src(src.sass)
    .pipe(sass({includePaths: [src.css_boots]}).on('error', gutil.log))
    .pipe(concat('all.css'))
    .pipe(gulp.dest(dest.css))
    //.pipe(uncss({html: ['templates/**/*.html', 'https://test1.inares.org/']}))
    .pipe(rename({suffix: '.min'}))
    .pipe(postcss([csswring]))
    //.pipe(sourcemap.write())
    .pipe(gulp.dest(dest.css))
    .pipe( notifyOK('CSS') );
});


gulp.task('js', function() {
  return gulp.src([
        configDir.source+'js/main.js',
        configDir.source+'js/webfont.js',
        configDir.bower+'jquery/dist/jquery.js',
        configDir.bower+'jquery.cookie/jquery.cookie.js',
        configDir.bower+'js-url/url.js',
        configDir.bower+'underscore/underscore.js',
        configDir.bower+'require/build/require.js',
        configDir.bower+'uri.js/src/URI.js',
        configDir.bower+'backbone/backbone.js',
        configDir.bower+'json2/json2.js',
        configDir.bower+'jquery-ui/jquery-ui.js',
        configDir.bower+'qTip2/jquery.qtip.js',
        configDir.bower+'swfobject/swfobject/swfobject.js',
//        configDir.bower+'jquery.bbq/jquery.ba-bbq.js',
        configDir.bower+'bootstrap-sass/assets/javascripts/bootstrap.js'
      ])
    .pipe(concat('main-vendor.js'))
    .pipe(gulp.dest(configDir.static+'js'))
    .pipe(uglify())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(configDir.static+'js'));
});


gulp.task('fonts', function() {
    return gulp.src(src.fonts)
    .pipe(gulp.dest(dest.fonts))
    .pipe( notifyOK('Font') );
});


gulp.task('images', function() {
  return gulp.src(src.images)
    .pipe(cache(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true })))
    .pipe(gulp.dest(dest.images))
    .pipe( notifyOK('Images') );
});


gulp.task('git', function(){
  return gulp.src(src.root)
    .pipe( git.add() );
});


gulp.task('assets', function(){
  exec('./scripts/edx-update-theme.sh', function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
  });
});


gulp.task('update-edx', function(){
  exec('./scripts/edx-update-plateforme.sh', function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
  });
});


gulp.task('sauv', function(){
  exec('tar -jcf /edx/app/edxapp/themes/edx-theme_backup_$(date +%Y-%m-%d_%H-%M).tar.bz2 .', function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
  });
});


// minify new or changed HTML pages
/*gulp.task('html', function() {
  gulp.src(src.templates)
    .pipe(changed(dest.templates))
    .pipe(minifyHTML())
    .pipe(gulp.dest(dest.templates))
    .pipe( notifyOK('HTML') );
});*/


gulp.task( 'default', ['css', 'fonts', 'images'] );

gulp.task( 'cssa', function() {
    runSequence('css', 'assets', function() { });
} );

gulp.task( 'jsa', function() {
    runSequence('js', 'assets', function() { });
} );


gulp.task( 'cjsa', function() {
    runSequence('js', 'css', 'assets', function() { });
} );